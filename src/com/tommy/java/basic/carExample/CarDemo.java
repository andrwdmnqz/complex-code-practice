package com.tommy.java.basic.carExample;

import com.tommy.java.basic.carExample.details.Engine;
import com.tommy.java.basic.carExample.professions.Driver;
import com.tommy.java.basic.carExample.vehicles.*;

public class CarDemo {
    public static void main(String[] args) {
        Driver bmwDriver = new Driver("Іванов В.В.", 50,  30);
        Engine bmwEngine = new Engine("60", "BMW");
        Car bmwCar = new Car();
        bmwCar.setProducer("BMW");
        bmwCar.setaClass("C");
        bmwCar.setWeight(5000);
        bmwCar.setDriver(bmwDriver);
        bmwCar.setEngine(bmwEngine);

        Driver lorryDriver = new Driver("Петров В.В.", 45,  20);
        Engine lorryEngine = new Engine("30", "LorryEngine");
        Lorry lorry = new Lorry("Вантажівка", "D", 8000, lorryDriver, lorryEngine, 70);

        Driver sportDriver = new Driver("Сидоров В.В.", 30,  15);
        Engine sportEngine = new Engine("80", "SportEngine");
        SportCar sportCar = new SportCar("SportCar", "C", 4000, sportDriver, sportEngine, 300);

        System.out.println("\n" + bmwCar);
        System.out.println("\n" + lorry);
        System.out.println("\n" + sportCar);
    }
}