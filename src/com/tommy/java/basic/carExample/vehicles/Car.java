package com.tommy.java.basic.carExample.vehicles;


import com.tommy.java.basic.carExample.details.Engine;
import com.tommy.java.basic.carExample.details.Trunk;
import com.tommy.java.basic.carExample.professions.Driver;

public class Car {
    private String producer;
    private String aClass;
    private double weight;
    private Driver driver;
    private Engine engine;
    private Trunk trunkStatus = Trunk.ЗАКРИТО;

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getaClass() {
        return aClass;
    }

    public void setaClass(String aClass) {
        this.aClass = aClass;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public void start() {
        System.out.println("Поїхали");
    }

    public void stop() {
        System.out.println("Зупиняємося");
    }

    public void turnRight() {
        System.out.println("Поворот направо");
    }

    public void turnLeft() {
        System.out.println("Поворот наліво");
    }

    public void turnHeadlightsOn() {
        System.out.println("Фари увіменено");
    }

    public void turnHeadlightsOff() {
        System.out.println("Фари вимкнено");
    }

    public void turnSignalsOn() {
        System.out.println("Поворотники увіменено");
    }

    public void turnSignalsOff() {
        System.out.println("Поворотники вимкнено");
    }

    public void turnLightOn() {
        System.out.println("Підсвітку увіменено");
    }

    public void turnLightOff() {
        System.out.println("Фари вимкнено");
    }

    public void openTrunk() {
        trunkStatus = Trunk.ВІДКРИТО;
    }

    public void closeTrunk() {
        trunkStatus = Trunk.ЗАКРИТО;
    }

    public void sportMode() {
        System.out.println("Встановлено спортивний режим їзди");
    }

    public void cityMode() {
        System.out.println("Встановлено міський режим їзди");
    }

    @Override
    public String toString() {
        return "Car{" +
                "producer='" + producer + '\'' +
                ", aClass='" + aClass + '\'' +
                ", weight=" + weight +
                ", driver=" + driver +
                ", engine=" + engine +
                ", trunkStatus=" + trunkStatus +
                '}';
    }
}